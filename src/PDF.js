import React, { Component } from "react";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  BlobProvider,
} from "@react-pdf/renderer";

// Create styles
const styles = StyleSheet.create({
  page: {
    backgroundColor: "#E4E4E4",
  },
  title: {
    fontSize: "16pt",
    fontWeight: "bold",
  },
  section: {
    margin: 2,
    padding: 5,
    textAlign: "center",
    fontSize: "12pt",
  },
});

export default class PDF extends Component {
  pdfText() {
    return (
      <Document>
        <Page size="A4" style={styles.page}>
          <View style={styles.section}>
            <Text style={styles.title}>Parodi Hujan</Text>
            <View style={styles.section}>
              <Text>Tak ada yang lebih basah</Text>
              <Text>dari hujan setelah Oktober. Banjir</Text>
            </View>
          </View>
        </Page>
      </Document>
    );
  }
  render() {
    return(
    <div style={{ height:'720px'}}>
        <BlobProvider document={this.pdfText()}>
            {({ url })=> <iframe src={url} style={{width:'100%', height:'100%'}}/>}
        </BlobProvider>
    </div>

    ) 
  }
}
